#!/usr/bin/python

import urllib,os,sys
import nltk
import codecs
from nltk.tokenize import *
from math import *


def tokenizeThat(t):
	return  regexp_tokenize(t,pattern=r'[a-zA-Z]+|[\d.]+|[,.?;]')

def constructDict(dico, text):
	
	liste =tokenizeThat(text)
	for word in liste :
		if word in dico :
			dico[word]=dico[word]+1
		else :
			dico[word]=1

def doVectorialSumFromDict(d1,d2):
	sum=0
	for word in d1 :
		if word in d2:
			sum+=d1[word]*d2[word]
	return sum
def vectorialLenghtFromDict(d):
	sum=0
	for word in d :		
			sum+=d[word]*d[word]
	print 'sum vect %s' % sum
	return sqrt(sum)





class text() :
	
	def __init__(self,texte=None,filename=None):
		self.dict={}
		self.filename=""
		self.texte=""
		if filename is None :
			
			self.texte=texte
			constructDict(self.dict, texte)
					
		else :
			self.filename=filename			
			f = codecs.open(filename, 'r3', encoding='utf-8')
			for text in f :
				constructDict(self.dict, text)

	def getName(self):
		if self.filename == "" :
			return None
		return self.filename
	def getWordTokens(self):
		liste=[]
		for key,value in self.dict.iteritems() :
			liste.append(key)
		return liste
	def getWeight(self,word) :
		return self.dict[word]



	def printDico(self):	
		print self.dict
	def cosineFromFilename(self,t):
		dict2={}

		f = codecs.open(t, 'r3', encoding='utf-8')
		for text in f :
			constructDict(dict2,text)
		longeur= doVectorialSumFromDict(self.dict,dict2)
		norme1= vectorialLenghtFromDict(self.dict)
		norme2= vectorialLenghtFromDict(dict2)
		print 'longueur = %s' % longeur
		print 'norme1 = %s' % norme1
		print 'norme2 = %s' % norme2
		return longeur/(norme2*norme1)

	def cosine(self,t):
		
		
		longeur= self.doVectorialSum(t)
		norme1= self.doVectorialLenght(self)
		norme2= self.doVectorialLenght(t)
		print 'longueur = %s' % longeur
		print 'norme1 = %s' % norme1
		print 'norme2 = %s' % norme2
	 	return longeur/(norme2*norme1)


	def doVectorialSum(self,d2):
		sum=0
		wordTokkend2=d2.getWordTokens()
		for word in self.getWordTokens() :
			if word in wordTokkend2:
				sum+=self.getWeight(word)*d2.getWeight(word)
		return sum
	def doVectorialLenght(self,d):
		sum=0
		wordTokkend=d.getWordTokens()
		for word in wordTokkend :		
		    sum+=d.getWeight(word)*d.getWeight(word)
		return sqrt(sum)
		



if __name__ == "__main__":
	if len(sys.argv) < 1:
		print "not enough argument : missing filename to parse"

	argument= sys.argv[1]
	c= text(filename=argument)
	#c.printDico()
	#print c.getName()
	#print c.getWordTokens()
	#print c.getWeight("manger")
	t=text(filename="./texte2")
	print c.cosine(t)
	print c.cosineFromFilename("./texte2")